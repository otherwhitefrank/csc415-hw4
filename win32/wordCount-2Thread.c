/*  Name: Frank Dye
Date: 3/12/2014
Description: A multi-threaded word count program using win32
*/


#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>


//Declarations for bool type
//typedef int bool;
//#define true 1
//#define false 0

#define BUFFER_SIZE 100 * 1024
#define NUM_THREADS 2
#define STRING_SIZE 512


//Open source file as read only, 
#define SOURCE_FLAGS O_RDONLY

//Forward declarations
DWORD WINAPI threadCountWords( LPVOID lpParam);
void wipeBuffer(char* buffer, char symbol, int maxBufferLength);
int findNextWhiteSpace(char* buffer, int startIndex, int maxBufferSize);
void processError(HRESULT errorCode);


//Thread data structure
struct thread_data {
	int threadId;
	int startIndex;
	int stopIndex;
	int* pThreadWordCount;
	char* buffer;
};


int main(int argc, char** argv) {

	//Create our 100K buffer
	char szBuffer[BUFFER_SIZE];

	//Source file descriptors
	HANDLE fdSourceFile = NULL;

	int threadStartIndex[NUM_THREADS];
	int threadStopIndex[NUM_THREADS];
	int threadWordCount[NUM_THREADS];

	int totalWordCount = 0;

	//Our thread handles
	HANDLE   hThreads[NUM_THREADS];
	DWORD   dwThreadIdArray[NUM_THREADS];
	//thread data 
	struct thread_data threadData[NUM_THREADS];

	if (argc != 2) {
		printf("Wrong number of arguments to wordCount!\n");
		exit(1);
	}

	//Open source file
	char szSourceFileName[STRING_SIZE];
	char szCWD[STRING_SIZE];

	int cwdLength = 0;
	int i = 0;
	
	//Acquire the current working directory
	cwdLength = GetCurrentDirectory(STRING_SIZE, szCWD);
	
	sprintf(szSourceFileName, "%s\\%s", szCWD, argv[1]);

	fdSourceFile = CreateFile((LPCWSTR) szSourceFileName, GENERIC_READ, 0, 
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
		NULL);

	//Check to see if file was created
	int errorCode = GetLastError();

	processError(errorCode); //exit if failed, otherwise continue.

	bool operationStatus = false;
	DWORD numBytesRead = 0;

	do {

		//Wipe buffer with space character
		wipeBuffer(szBuffer, ' ', BUFFER_SIZE);


		//Read 512 bytes from the file
		operationStatus = ReadFile(fdSourceFile, &szBuffer, BUFFER_SIZE, &numBytesRead, NULL);


		if (!operationStatus)
		{
			//Read failed so process the error
			errorCode = GetLastError();

			processError(errorCode);
		}

		int bufferDivider = 0;

		//Our partition of the buffer
		bufferDivider = numBytesRead / NUM_THREADS;

		int tempBufferBoundary = 0;
		int tempStartIndex = 0;


		//Declare our index variables
		
		//Boundaries for Pthreads
		for (i = 0; i < NUM_THREADS; i++) {
			tempBufferBoundary = (i + 1) * bufferDivider;
			threadStartIndex[i] = tempStartIndex;
			tempBufferBoundary = findNextWhiteSpace(szBuffer, tempBufferBoundary, BUFFER_SIZE);
			threadStopIndex[i] = tempBufferBoundary;
			tempStartIndex = threadStopIndex[i] + 1;
		}

		//Setup out threadData struct
		for (i = 0; i < NUM_THREADS; i++) {
			threadData[i].buffer = szBuffer;
			threadData[i].threadId = i;
			threadData[i].startIndex = threadStartIndex[i];
			threadData[i].stopIndex = threadStopIndex[i];
			threadData[i].pThreadWordCount = &threadWordCount[i];
		}

		//Create out threads and hand them the threadData struct saving their thread handle
		int rc = 0;
		for (i = 0; i < NUM_THREADS; i++) 
		{
			hThreads[i] = CreateThread( 
				NULL,                   // default security attributes
				0,                      // use default stack size  
				threadCountWords,       // thread function name
				(LPVOID*) &threadData[i],          // argument to thread function 
				0,                      // use default creation flags 
				&dwThreadIdArray[i]);   // returns the thread identifier 

			if (hThreads[i] == NULL) 
			{
				printf("Error creating thread %d\n", i);
				ExitProcess(3);
			}

			
		}

		// Wait until all threads have terminated.

		WaitForMultipleObjects(NUM_THREADS, hThreads, TRUE, INFINITE);


		//Add the total to our wordCount variable
		for (i = 0; i < NUM_THREADS; i++)
		{
			totalWordCount += threadWordCount[i];
		}

	} while (!(numBytesRead < BUFFER_SIZE));

	//Finished code here, calculate totals and output them

	printf("\n%d words\n", totalWordCount);

	//Close all are threads
	for (int i = 0; i < NUM_THREADS; i++)
	{
        CloseHandle(hThreads[i]);
	}
	/* Last thing that main() should do */
	return 0;
}

//Our thread function to cound the words of a shared buffer
DWORD WINAPI threadCountWords( LPVOID lpParam) {

	//data that is passed in to the thread as a void* ptr
	struct thread_data *tData;

	//Cast the pointer
	tData = (struct thread_data*) lpParam;
	//Variable setup
	int wordCount = 0;
	bool hitSpace = false;
	bool nextSpace = false;
	int i = tData->startIndex;


	//printf("\ntData: threadID: %d\nthreadStart: %d\nthreadStop: %d\n", tData->threadId, tData->startIndex, tData->stopIndex);
	//Does our buffer start on a word or a space
	if ((bool) isspace(tData->buffer[i]))
	{
		hitSpace = true;
	}
	else
	{
		hitSpace = false;
		wordCount++;
	}

	for (; i < tData->stopIndex; i++) {
		nextSpace = (bool) isspace(tData->buffer[i]);
		if (hitSpace == true && nextSpace == false) {
			//If we have hit a space and find a non-space then we have encountered a new word so increment wordCount
			//and set hitSpace to false
			wordCount++;
		}
		hitSpace = nextSpace;
	}

	//printf("\nThread %ld done. Result = %d\n", tData->threadId, wordCount);
	*(tData->pThreadWordCount) = wordCount;

	return 0;
	
}


void wipeBuffer(char* buffer, char symbol, int maxBufferLength) {
	int i = 0;
	for (i = 0; i < maxBufferLength; i++) {

		buffer[i] = symbol;
	}
}

int findNextWhiteSpace(char* buffer, int startIndex, int maxBufferSize) {
	int currIndex = startIndex;
	while (!isspace(buffer[currIndex]) && currIndex < maxBufferSize) {
		currIndex++;
	}

	return currIndex;
}

//Helper function to process error codes 
void processError(HRESULT errorCode)
{

	if (errorCode == ERROR_FILE_NOT_FOUND)
	{
		printf("Source File Not Found!\n");
		exit(1);
	}
	else if (errorCode == ERROR_FILE_EXISTS)
	{
		printf("Destination File Already Exists!\n");
		exit(1);
	}
	else if (errorCode == ERROR_IO_PENDING)
	{
		printf("IO Error reading/writing!");
		exit(1);
	}
	else if (errorCode == ERROR_SUCCESS)
	{
		//No error code, ignore
	}
	else if (errorCode == S_OK)
	{
		//No error, file opened okay, ignore
	}
	else
	{
		printf("Error in program!\n");
		exit(1);
	}
}