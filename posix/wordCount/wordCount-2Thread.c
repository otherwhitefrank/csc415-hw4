/*  Name: Frank Dye
    Date: 3/12/2014
    Description: A multi-threaded word count program using posix
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>


#define BUFFER_SIZE 100 * 1024
#define NUM_THREADS 2

//Open source file as read only, 
#define SOURCE_FLAGS O_RDONLY


//Thread data structure

struct thread_data {
    int threadId;
    int startIndex;
    int stopIndex;
    int* pThreadWordCount;
    char* buffer;
};



//Fix this for final touch.
void* pthreadCountWords(void *t) {

    //data that is passed in to the thread as a void* ptr
    struct thread_data *tData;

    //Cast the pointer
    tData = (struct thread_data*) t;
    //Variable setup
    int wordCount = 0;
    bool hitSpace = false;
    bool nextSpace = false;
    int i = tData->startIndex;
    
    //Does our buffer start on a word or a space
    if ((bool) isspace(tData->buffer[i]))
    {
        hitSpace = true;
    }
    else
    {
        hitSpace = false;
        wordCount++;
    }
    
    for (; i < tData->stopIndex; i++) {
        nextSpace = (bool) isspace(tData->buffer[i]);
        if (hitSpace == true && nextSpace == false) {
            //If we have hit a space and find a non-space then we have encountered a new word so increment wordCount
            //and set hitSpace to false
            wordCount++;
        }
        hitSpace = nextSpace;
    }

    //printf("Thread %ld done. Result = %d\n", tData->threadId, wordCount);
    *(tData->pThreadWordCount) = wordCount;
    pthread_exit((void*) t);
}

void processError(int currentError) {
    //strerror() prints out a char* message that corresponds to the error 

    printf("%s", strerror(currentError));
    exit(1);
}

void wipeBuffer(char* buffer, char symbol, int maxBufferLength) {
    int i = 0;
    for (i = 0; i < maxBufferLength; i++) {

        buffer[i] = symbol;
    }
}

int findNextWhiteSpace(char* buffer, int startIndex, int maxBufferSize) {
    int currIndex = startIndex;
    while (!isspace(buffer[currIndex]) && currIndex < maxBufferSize) {
        currIndex++;
    }

    return currIndex;
}

int main(int argc, char** argv) {

    char szBuffer[BUFFER_SIZE];

    int numBytesRead = 0;

    //Source file descriptors
    int fdSourceFile = -1;

    int threadStartIndex[NUM_THREADS];
    int threadStopIndex[NUM_THREADS];
    int threadWordCount[NUM_THREADS];

    int totalWordCount = 0;

    //Our pthread handles
    pthread_t threads[NUM_THREADS];
    //pthread data 
    struct thread_data threadData[NUM_THREADS];
    //pthread attr
    pthread_attr_t attr;
    /* Initialize and set thread detached attribute */
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    //void* status for return value
    void *status;


    if (argc != 2) {
        printf("Wrong number of arguments to wordCount!\n");
        exit(1);
    }

    //Open source file
    fdSourceFile = open(argv[1], SOURCE_FLAGS);

    //Check for error opening sourceFileName
    if (fdSourceFile == -1) {
        processError(errno);
    }

    do {

        //Wipe buffer with space character
        wipeBuffer(szBuffer, ' ', BUFFER_SIZE);

        //Read BUFFER_SIZE bytes from the file
        numBytesRead = read(fdSourceFile, &szBuffer, BUFFER_SIZE);

        if (numBytesRead == -1) {
            //Error reading from file
            processError(errno);

            //Exit with error
            exit(1);
        }

        int bufferDivider = 0;

        //Our partition of the buffer
        bufferDivider = numBytesRead / NUM_THREADS;

        int tempBufferBoundary = 0;
        int tempStartIndex = 0;


        //Declare our index variables
        int i = 0;
        //Boundaries for Pthreads
        for (i = 0; i < NUM_THREADS; i++) {
            tempBufferBoundary = (i + 1) * bufferDivider;
            threadStartIndex[i] = tempStartIndex;
            tempBufferBoundary = findNextWhiteSpace(szBuffer, tempBufferBoundary, BUFFER_SIZE);
            threadStopIndex[i] = tempBufferBoundary;
            tempStartIndex = threadStopIndex[i] + 1;
        }

        //Setup out threadData struct
        for (i = 0; i < NUM_THREADS; i++) {
            threadData[i].buffer = szBuffer;
            threadData[i].threadId = i;
            threadData[i].startIndex = threadStartIndex[i];
            threadData[i].stopIndex = threadStopIndex[i];
            threadData[i].pThreadWordCount = &threadWordCount[i];
        }

        //Create out threads and hand them the threadData struct saving their thread handle
        int rc = 0;
        for (i = 0; i < NUM_THREADS; i++) {
            rc = pthread_create(&threads[i], &attr, pthreadCountWords, (void *) &threadData[i]);

            if (rc) {
                printf("ERROR; return code from pthread_create() is %d\n", rc);
                exit(-1);
            }
        }

        /* Free attribute and wait for the other threads */
        pthread_attr_destroy(&attr);

        for (i = 0; i < NUM_THREADS; i++) {
            rc = pthread_join(threads[i], &status);
            if (rc) {
                printf("ERROR; return code from pthread_join() is % d\n", rc);
                exit(-1);
            }
        }
        
        //Add the total to our wordCount variable
        for (i = 0; i < NUM_THREADS; i++)
        {
            totalWordCount += threadWordCount[i];
        }
        
        

    } while (!(numBytesRead < BUFFER_SIZE));

    //Finished code here, calculate totals and output them

    printf("%d words\n", totalWordCount);
    
    /* Last thing that main() should do */
    pthread_exit(NULL);
}
